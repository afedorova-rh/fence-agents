#!/bin/sh
export PYTHON_KEYRING_BACKEND="keyring.backends.null.Keyring"

find -maxdepth 1 -not -name "fence-agents-*.tar.gz" -and -not -name "awscli-*.tar.gz" -and -not -name "botocore-2*.zip" -and \( -name "*.whl" -or -name "*.tar.?z*" \) -delete
sed -i -n -E '/\(fence-agents-|\(awscli-|\(botocore-2/p' sources
sha512sum --tag requirements-*.txt >> sources

for x in requirements-*.txt; do
	echo "# $x" | sed -E "s/requirements-(.*).txt/\1/" >> sources
	pip download --no-binary msgpack,netifaces -r $x | awk '/Saved/{gsub("./", "", $2); print $2}' | sort | xargs sha512sum --tag >> sources
done

awk 'NR<11{next} /^# /{print}; /^[^#]/{gsub("[()]", "", $2); printf "Source%d: %s\n", 1000+c++, $2}' sources

sed -i '/^#/d' sources

if ! git diff --quiet sources; then
	cat << EOF

Upload new sources by running:
awk '/^[^#]/{gsub("[()]", "", \$2); printf "%s ", \$2}' sources | xargs centpkg new-sources

EOF
fi
